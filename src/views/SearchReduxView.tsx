import React from "react"
import { Album } from "../models/Album"
import { connect, useDispatch } from "react-redux"
import { SearchForm } from "../components/SearchForm"
import { SearchResults } from "../components/SearchResults"
import { searchAlbums, SearchState } from "../reducers/search.reducer"
import { bindActionCreators } from "redux"

import { RouteComponentProps } from 'react-router-dom'
type State = {
    search: SearchState
}

const ReduxSearchForm = connect(
    (state: State) => ({
        results: state.search.query
    }),
    (dispatch: any) => ({
        onSearch(q: string) {
            // redux-thunk
            dispatch(searchAlbums(q))
        }
    })
)(SearchForm)

const ReduxSearchResults = connect(
    (state: State) => ({
        results: state.search.results,
        error: state.search.error,
        isLoading: state.search.isLoading
    }),
    // dispatch => bindActionCreators({
    //     onSearch: searchAlbums
    // }, dispatch)
)(SearchResults)


type P = {} & RouteComponentProps<{ param: string }>

export const SearchReduxView: React.FC<P> = (props) => {
    const dispatch = useDispatch()

    const p = new URLSearchParams(props.location.search)
    const q = p.get('query')
    if (q) {
        dispatch(searchAlbums(q))
    }

    return <div>
        <div className="row">
            <div className="col">
                <ReduxSearchForm />
            </div>
        </div>

        <div className="row">
            <div className="col">
                <ReduxSearchResults />
            </div>
        </div>
    </div>
}