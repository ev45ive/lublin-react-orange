import React from "react";
import { SearchForm } from "../components/SearchForm";
import { SearchResults } from "../components/SearchResults";
import { Album } from "../models/Album";
import { search } from "../services";

type S = {
  query: string
  results: Album[]
}
type P = {}

const mockResults: Album[] = [
  {
    id: '123', name: 'Test 123', images: [
      { url: 'https://www.placecage.com/gif/300/300' }
    ]
  }, {
    id: '243', name: 'Test 234', images: [
      { url: 'https://www.placecage.com/gif/400/400' }
    ]
  }, {
    id: '345', name: 'Test 345', images: [
      { url: 'https://www.placecage.com/gif/500/500' }
    ]
  }
]

export class SearchView extends React.Component<P, S>{
  state: S = {
    query: 'batman',
    results: mockResults
  }
  
  componentDidMount() {
    this.search('batman')
  }

  search = (query: string) => {
    this.setState({ query })

    search.searchAlbums(query)
      .then(albums => this.setState({
        results: albums
      }))
  }

  render() {
    return (
      <div>
        <div className="row">
          <div className="col">
            <SearchForm onSearch={this.search} />
          </div>
        </div>

        <div className="row">
          <div className="col">
            <SearchResults results={this.state.results} />
          </div>
        </div>
      </div>
    )
  }
}