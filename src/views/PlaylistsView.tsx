import React from "react";
import { ItemsList } from "../components/ItemsList";
import { PlaylistDetails } from "../components/PlaylistDetails";
import { PlaylistForm } from "../components/PlaylistForm";
import { Playlist } from "../models/Playlist";
import { store } from "../store";
import { Unsubscribe } from "redux";

type S = {
  mode: 'show' | 'edit'
  query: string,
  playlists: Playlist[]
  selected: Playlist | null
}

export class PlaylistsView extends React.Component<{}, S> {
  state: S = {
    query: '',
    mode: 'show',
    selected: null,
    playlists: [
      /* Poszły do Reduxa! ===> */
    ],
  }

  unsubscribeRedux!: Unsubscribe

  componentDidMount() {
    const { playlists: { items: playlists, selected } } = store.getState()
    this.setState({
      playlists,
      selected: playlists.find(p => p.id == selected) || null
    })

    this.unsubscribeRedux = store.subscribe(() => {
      const { playlists: { items: playlists, selected } } = store.getState()
      this.setState({
        playlists,
        selected: playlists.find(p => p.id == selected) || null
      })
    })
  }
  
  componentWillUnmount() {
    this.unsubscribeRedux()
  }




  edit = () => { this.setState({ mode: 'edit' }) }
  cancel = () => { this.setState({ mode: 'show' }) }

  save = (draft: Playlist) => {
    this.setState({
      // Create copy of playlists with updated draft!
      playlists: this.state.playlists.map((playlist) =>
        playlist.id === draft.id ? draft : playlist
      ),
      selected: draft,
      mode: 'show'
    })
  }

  select = (selected: Playlist) => {
    this.setState({ selected: this.state.selected === selected ? null : selected })
  }

  render() {
    return (
      <div>
        <div className="row">
          <div className="col">
            <input placeholder="Search" className="form-control mb-3"
              onChange={e => this.setState({ query: e.currentTarget.value })} />

            <ItemsList
              playlists={this.state.playlists}
              selected={this.state.selected}
              onSelect={this.select} />

          </div>

          <div className="col">

            {this.state.selected && this.state.mode === 'show' && <div>
              <PlaylistDetails
                onEdit={this.edit}
                playlist={this.state.selected} />
            </div>}

            {this.state.selected && this.state.mode === 'edit' && <div>
              <PlaylistForm
                playlist={this.state.selected}
                onCancel={this.cancel}
                onSave={this.save} />
            </div>}

            {!this.state.selected && <p>Please select playlist</p>}
          </div>

        </div>
      </div>
    )
  }
}