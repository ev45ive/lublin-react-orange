import React from "react"
import { SearchForm } from "../components/SearchForm"
import { SearchResults } from "../components/SearchResults"
import { SearchCtx } from "../services"

export const SearchViewCtx: React.FC = () => {
  return (
    <div>
      <div className="row">
        <div className="col">
          <SearchCtx.Consumer>{
            ({ onSearch }) => <SearchForm onSearch={onSearch} />
          }</SearchCtx.Consumer>
        </div>
      </div>

      <div className="row">
        <div className="col">
          <SearchCtx.Consumer>{
            ({ results }) => <SearchResults results={results} />
          }</SearchCtx.Consumer>
        </div>
      </div>
    </div>
  )
}