import React, { useState } from "react";
import { PlaylistsList } from "../containers/PlaylistsList";
import { SelectedPlaylist, SelectedPlaylistForm } from "../containers/SelectedPlaylist";
import { useStore, useSelector, useDispatch } from "react-redux";
import { Prompt, Redirect, RouteComponentProps, Route } from "react-router-dom";
import { playlistsSelect } from "../reducers/playlists";

type P = {} & RouteComponentProps<{ playlist_id: string }>

export const PlaylistsReduxView: React.FunctionComponent<P> = ({ match, history }) => {
    const [mode, setMode] = useState<'show' | 'edit'>('show')
    const dispatch = useDispatch()

    if (match.params.playlist_id) {
        dispatch(playlistsSelect(match.params.playlist_id))
        // dispatch(fetchPlaylistAndSelect(match.params.playlist_id))
    }

    // const selected = useStore().getState().playlists.selected
    const selected = useSelector((state: any) => state.playlists.selected)

    return <div>
        <div className="row">

            <div className="col">
                <PlaylistsList onSelect={(p) => history.replace('/playlists/' + p.id)} />
            </div>

            <div className="col">
                {selected && mode == 'show' && <SelectedPlaylist onEdit={() => setMode('edit')} />}

                {selected && mode == 'edit' && <SelectedPlaylistForm onCancel={() => setMode('show')} />}


            </div>
        </div>
    </div>
}