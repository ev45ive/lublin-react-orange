import React from 'react';
import 'bootstrap/dist/css/bootstrap.css'
import { PlaylistsReduxView } from './views/PlaylistsReduxView';
import { SearchReduxView } from './views/SearchReduxView';
import { Route, Switch, NavLink } from 'react-router-dom';

const Layout: React.FC = ({ children }) => {
  return (
    <div className="row">
      <div className="col">
        {children}
      </div>
    </div>
  )
}

const App: React.FC = () => {
  return (
    <div className="App">
      <nav className="navbar navbar-expand navbar-dark bg-dark mb-3">
        <div className="container">
          <NavLink className="navbar-brand" activeClassName="placki active" to="/">Music React</NavLink>

          <div className="collapse navbar-collapse">
            <ul className="navbar-nav">

              <li className="nav-item">
                <NavLink className="nav-link" to="/playlists">Playlists</NavLink>
              </li>

              <li className="nav-item">
                <NavLink className="nav-link" to="/search">Search Music</NavLink>
              </li>

            </ul>
          </div>
        </div>
      </nav>




      <div className="container">
        <div className="row">
          <div className="col">

            <Switch>
              <Route path="/" exact={true} component={PlaylistsReduxView} />
              <Route path="/playlists/:playlist_id" component={PlaylistsReduxView} />
              <Route path="/playlists" component={PlaylistsReduxView} />
              <Route path="/search" component={SearchReduxView} />
              <Route path="*" render={() => <p>404 not found</p>} />
            </Switch>

          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
