import { createStore, combineReducers, applyMiddleware } from 'redux'
import { playlists as playlistsReducer } from './reducers/playlists';
import { searchReducer } from './reducers/search.reducer';

import thunk from 'redux-thunk'

const reducer = combineReducers({
    // counter: counter,
    playlists: playlistsReducer,
    search: searchReducer
})


export const store = createStore(reducer, applyMiddleware(thunk));

; (window as any).store = store