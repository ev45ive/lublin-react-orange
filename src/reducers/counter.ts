import { Reducer, AnyAction, Action, ActionCreator } from "redux";

type State = number

export const counter: Reducer<State, CounterActions> = (state = 0, action) => {
    switch (action.type) {
        case 'INCREMENT': return state + action.payload.amount
        case 'DECREMENT': return state + action.payload.amount
        default:
            return state
    }
}

type CounterActions = INC | DEC

interface INC {
    type: 'INCREMENT'
    payload: { amount: number }
}

interface DEC extends Action<'DECREMENT'> {
    payload: { amount: number }
}

// store.dispatch(incAction(12))

export const incAction: ActionCreator<INC> = (amount = 1) => {
    if (amount <= 0) { throw 'Invalid inc amount' }

    return {
        type: 'INCREMENT',
        payload: {
            amount
        }
    }
}

export const decAction: ActionCreator<DEC> = (amount = 1) => ({
    type: 'DECREMENT',
    payload: {
        amount
    }
})

;(window as any).inc = incAction;
;(window as any).dec = decAction;