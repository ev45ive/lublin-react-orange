
import { Action, ActionCreator, Reducer, Dispatch } from "redux"
import { Album } from "../models/Album"
import { search, auth } from "../services"

export type SearchState = {
    query: string,
    status: 'START' | 'LOADING' | 'SUCCESS' | 'ERROR',
    isLoading: boolean,
    error: Error | null,
    results: Album[]
}

const initialState: SearchState = {
    query: '',
    status: 'START',
    isLoading: false,
    error: null,
    results: [],
}

export const searchReducer: Reducer<SearchState, ActionTypes> = (
    state = initialState,
    action
) => {
    switch (action.type) {
        case 'SEARCH_START': return {
            ...state,
            query: action.payload.query, results: [], isLoading: true
        }
        case 'SEARCH_SUCCESS': return {
            ...state,
            results: action.payload, isLoading: false
        }
        case 'SEARCH_FAILED': return {
            ...state,
            error: action.payload, isLoading: false
        }
        default: return state
    }
}

type ActionTypes = SEARCH_START | SEARCH_SUCCESS | SEARCH_FAILED;

interface SEARCH_START extends Action<'SEARCH_START'> { payload: { query: string } }
interface SEARCH_SUCCESS extends Action<'SEARCH_SUCCESS'> { payload: Album[] }
interface SEARCH_FAILED extends Action<'SEARCH_FAILED'> { payload: Error }

const searchStart = (query: string) => ({ type: 'SEARCH_START', payload: { query } })
const searchSuccess = (albums: Album[]) => ({ type: 'SEARCH_SUCCESS', payload: albums })
const searchFailed = (error: Error) => ({ type: 'SEARCH_FAILED', payload: error })

export const searchAlbums = (query: string) => (dispatch: Dispatch) => {

    dispatch(searchStart(query))
    search
        .searchAlbums(query)
        .then(albums => dispatch(searchSuccess(albums)))
        .catch(error => {
            dispatch((searchFailed(error.response.data.error)))
        })

    // redux-promise
    // return search
    //     .searchAlbums(query).then(albums => searchSuccess(albums))
}