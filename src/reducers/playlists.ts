import { Reducer, Action, ActionCreator } from "redux"
import { Playlist } from "../models/Playlist"

type State = {
    items: Playlist[],
    selected: Playlist['id'] | null
}

const initialState: State = {
    items: [{
        type: 'Playlist',
        id: 123,
        name: 'Redux Hits',
        color: '#ff00ff',
        favorite: true,
    }, {
        type: 'Playlist',
        id: 234,
        name: 'React Top20',
        color: '#ffff00',
        favorite: false,
    }, {
        type: 'Playlist',
        id: 345,
        name: 'Best of React',
        color: '#00ffff',
        favorite: false,
    }],
    selected: null
}

export const playlists: Reducer<State, ActionTypes> = (
    state = initialState,
    action
) => {
    switch (action.type) {
        case 'PLAYLISTS_LOAD': return { ...state, items: action.payload }
        case 'PLAYLISTS_SELECT': return { ...state, selected: action.payload.id }
        case 'PLAYLISTS_UPDATE': {
            const draft = action.payload;
            return { ...state, items: state.items.map(p => p.id === draft.id ? draft : p) }
        }
        default: return state
    }
}

type ActionTypes = PLAYLISTS_SELECT | PLAYLISTS_LOAD | PLAYLISTS_UPDATE

interface PLAYLISTS_LOAD extends Action<'PLAYLISTS_LOAD'> {
    payload: Playlist[]
}
interface PLAYLISTS_SELECT extends Action<'PLAYLISTS_SELECT'> {
    payload: { id: Playlist['id'] | null }
}
interface PLAYLISTS_UPDATE extends Action<'PLAYLISTS_UPDATE'> {
    payload: Playlist
}

export const playlistsSelect: ActionCreator<PLAYLISTS_SELECT> =
    (id: Playlist['id']) => ({
        type: 'PLAYLISTS_SELECT', payload: { id: parseInt(id.toString()) }
    })

export const playlistsUpdate: ActionCreator<PLAYLISTS_UPDATE> =
    (payload: Playlist) => ({
        type: 'PLAYLISTS_UPDATE', payload
    })