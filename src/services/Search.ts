import axios from 'axios'
import { AlbumsResponse } from '../models/Album'

export class SearchService {

  constructor(private url: string) { }

  searchAlbums(query = 'batman') {

    return axios.get<AlbumsResponse>(this.url, {
      params: {
        type: 'album',
        q: query
      }
    })
    .then(res => res.data.albums.items)
  }
}