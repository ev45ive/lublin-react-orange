

export class AuthService {

    token: string | null = null

    constructor(
        private auth_url: string,
        private client_id: string,
        private response_type: string,
        private redirect_uri: string
    ) {
        const rawToken = sessionStorage.getItem('token')
        if (rawToken) {
            this.token = JSON.parse(rawToken)
        }

        if (!this.token && window.location.hash) {
            const p = new URLSearchParams(window.location.hash)
            this.token = p.get('#access_token')
            window.location.hash = ''
        }
        if (this.token) {
            sessionStorage.setItem('token', JSON.stringify(this.token))
        }
    }

    authorize() {

        const p = new URLSearchParams({
            client_id: this.client_id,
            response_type: this.response_type,
            redirect_uri: this.redirect_uri,
            // show_dialog: 'true'
        })

        const url = `${this.auth_url}?${p.toString()}`
        window.location.replace(url)
    }

    getToken() {
        if (!this.token) {
            this.authorize()
        }
        return this.token
    }
}