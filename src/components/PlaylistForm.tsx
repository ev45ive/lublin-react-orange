import React from "react";
import { Playlist } from "../models/Playlist";

type P = {
    playlist: Playlist
    onCancel(): void
    onSave(draft: Playlist): void
}
type S = {
    playlist: Playlist
}
export class PlaylistForm extends React.PureComponent<P, S> {

    state: S = {
        playlist: {
            type: 'Playlist',
            id: 0,
            name: ' ',
            color: '#ffffff',
            favorite: true,
        }
    }

    constructor(props: P) {
        super(props)
        // this.state.playlist = props.playlist
        console.log('constructor')
    }

    static getDerivedStateFromProps(newProps: P, newState: S) {
        console.log('getDerivedStateFromProps')
        return {
            playlist: newProps.playlist.id === newState.playlist.id ?
                newState.playlist : newProps.playlist
        }
    }

    // shouldComponentUpdate(newProps: P, newState: S) {
    //     console.log('shouldComponentUpdate')
    //     return this.props.playlist !== newProps.playlist
    //         || this.state.playlist !== newState.playlist
    // }

    getSnapshotBeforeUpdate(prevProps = {}, prevState = {}) {
        console.log('getSnapshotBeforeUpdate')
        return { placki: 123 }
    }

    componentDidUpdate(props: P, state: S, snapshot: any) { console.log('componentDidUpdate', snapshot) }

    componentDidMount() {
        console.log('componentDidMount')
        if (this.nameInputRef.current) {
            this.nameInputRef.current.focus()
        }
    }

    nameInputRef = React.createRef<HTMLInputElement>()

    componentWillUnmount() { console.log('componentWillUnmount') }

    save = () => {
        this.props.onSave(this.state.playlist)
    }

    handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const target = event.currentTarget
        const value = target.type === 'checkbox' ? target.checked : target.value
        const fieldName = target.name

        this.setState((prevState) => ({
            playlist: {
                ...prevState.playlist,
                [fieldName]: value,
            }
        }))
    }

    render() {
        console.log('render')
        return (
            <form>
                {/* <pre>{JSON.stringify(this.state.playlist,null,2)}</pre> */}
                <div className="form-group">
                    <label>Name:</label>
                    <input type="text" className="form-control"
                        ref={this.nameInputRef}
                        value={this.state.playlist.name}
                        onChange={this.handleChange}
                        name="name" />

                    {170 - this.state.playlist.name.length} / 170
                </div>

                <div className="form-group">
                    <label>Favorite:</label>
                    <input type="checkbox"
                        checked={this.state.playlist.favorite}
                        onChange={this.handleChange}
                        name="favorite" />
                </div>

                <div className="form-group">
                    <label>Color:</label>
                    <input type="color"
                        value={this.state.playlist.color}
                        onChange={this.handleChange}
                        name="color" />
                </div>

                <input type="button" value="Cancel" onClick={this.props.onCancel} />
                <input type="button" value="Save" onClick={this.save} />
                {/* <input type="button" value="Save" onClick={() => this.props.onSave(this.state.playlist)} /> */}

            </form>
        )
    }
}