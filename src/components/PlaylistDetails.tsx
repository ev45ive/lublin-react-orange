import React from "react";
import { Playlist } from "../models/Playlist";

type P = {
    playlist: Playlist
    onEdit(): void
}

export const PlaylistDetails: React.FunctionComponent<P>
    = React.memo(({ playlist, onEdit }) => (
        <div>
            <dl>
                <dt>Name:</dt>
                <dd>{playlist.name}</dd>

                <dt>Favorite:</dt>
                <dd>
                    {playlist.favorite ?
                        'Yes' : 'No'
                    }
                </dd>

                <dt>Color:</dt>
                <dd style={
                    {
                        color: playlist.color,
                        backgroundColor: playlist.color
                    }
                }>{playlist.color}</dd>
            </dl>
            <input type="button" value="Edit" onClick={onEdit} />

        </div>
    )/* propsComparator */)
