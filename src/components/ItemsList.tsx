import React from "react";
import { Playlist } from "../models/Playlist";


type P = {
  playlists: Playlist[]
  selected: Playlist | null,
  onSelect(selected: Playlist): void
}

export class ItemsList extends React.PureComponent<P> {

  render() {
    return (
      <div>
        <div className="list-group">
          {this.props.playlists.map((playlist, index) => {
            const activeClass = this.props.selected?.id === playlist.id ? 'active' : ''

            return <div className={"list-group-item " + activeClass} key={playlist.id}
              onClick={() => this.props.onSelect(playlist)}>
              {index + 1}. {playlist.name}
            </div>
          })}
        </div>
      </div>
    )
  }
}