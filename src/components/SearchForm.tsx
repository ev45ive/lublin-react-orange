import React from "react";
import { Prompt } from "react-router-dom";

type S = {
  query: string,
}
type P = {
  onSearch(q: string): void
}

export class SearchForm extends React.Component<P, S>{
  state = { query: '' }
  handleKeyUp = (e: React.KeyboardEvent<HTMLInputElement>) => {
    if (e.key === 'Enter') {
      this.search()
    }
  }

  search = () => {
    this.props.onSearch(this.state.query)
  }

  render() {
    return (
      <div className="input-group mb-3">

        {/* {true? <Redirect to="/search" /> } */}

        <Prompt
          when={this.state.query !== ''}
          message="Are you sure you want to leave?"
        />


        <input type="text" className="form-control" placeholder="Search"
          onKeyUp={this.handleKeyUp}
          onChange={e => this.setState({ query: e.currentTarget.value })} />

        <div className="input-group-append">
          <button className="btn btn-outline-secondary" type="button"
            onClick={this.search}>
            Search
            </button>
        </div>
      </div>
    )
  }
}