import React from "react"
import { AlbumCard } from "./AlbumCard"
import { Album } from "../models/Album"

import './SearchResults.css'

type P = {
    results: Album[],
    isLoading?: boolean
    error?: Error | null
}

export const SearchResults: React.FC<P> = ({ results, error, isLoading }) =>
    <div className="card-group">

        {error?.message}

        {isLoading && <p>Please wait loading...</p>}

        {results.map(result =>
            <AlbumCard album={result} key={result.id} />
        )}
    </div>
