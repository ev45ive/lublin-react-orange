import { SearchService } from "./services/Search";
import { AuthService } from "./services/Auth";
import axios from 'axios'
import React from "react";
import { Album } from "./models/Album";


export const search = new SearchService(
    'https://api.spotify.com/v1/search'
)

export const auth = new AuthService(
    'https://accounts.spotify.com/authorize',
    '70599ee5812a4a16abd861625a38f5a6',
    'token',
    'http://localhost:3000/'
)

axios.interceptors.request.use((config) => {
    config.headers['Authorization'] = 'Bearer ' + auth.getToken()
    return config
})
axios.interceptors.response.use(ok => ok,
    //catch 
    error => {

        if (error.response.status === 401) {
            sessionStorage.removeItem('token')
            auth.authorize()
        }
        // throw error
        return Promise.reject(error)
    })

export const SearchCtx = React.createContext<{
    query: string
    results: Album[]
    onSearch(q: string): void
}>({
    query: '',
    results: [],
    onSearch() {
        throw Error('No context provider')
    }
})




// git clone https:// bitbucket.org/ ev45ive/ 


// placki@placki.com
// ******