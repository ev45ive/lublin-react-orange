
export interface Album {
  id: string
  name: string
  images: AlbumImage[]
}

export interface AlbumImage {
  url: string
}

export interface PagingObject<T> {
  items: T[]
}

export interface AlbumsResponse {
  albums: PagingObject<Album>
}