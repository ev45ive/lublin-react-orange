
export interface Entity {
    id: number
    name: string
}

export interface Playlist extends Entity {
    type: 'Playlist'
    favorite: boolean
    /**
     * HEX color
     */
    color: string
    tracks?: Track[]
}

export interface Track extends Entity { 
    type: 'Track'
}

// ====

// const x: Playlist | Track = {}

// if(x.type === 'Playlist' ){
//     // x.tracks && x.tracks.length
//     x.tracks?.length
// }else{
//     x
// }






// ====

// const p: Playlist = {
//     id: 13,
//     name: 'PLayli',
//     favorite: true,
//     color: '#ff00ff',
//     tracks: [
//         {
//             id: 123, name: 'Track'
//         }
//     ]
// }