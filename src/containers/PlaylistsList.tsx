import { connect, MapStateToPropsParam, MapDispatchToPropsParam } from "react-redux";
import { ItemsList } from "../components/ItemsList";
import { Playlist } from "../models/Playlist";
import { playlistsSelect } from "../reducers/playlists";
import { RouteComponentProps } from "react-router-dom";

// Redux
type State = {
    playlists: {
        items: Playlist[],
        selected: Playlist['id']
    }
}

export const PlaylistsList = connect(
    // map State to Props
    (state: State) => ({
        playlists: state.playlists.items,
        selected: state.playlists.items.find(p => p.id === state.playlists.selected) || null
    }),

    // map Callback Props to Dispatch
    (dispatch) => ({
        // onSelect(p: Playlist) {
        //     dispatch(playlistsSelect(p.id))
        // }
    }))
    // Wrap component
    (ItemsList)