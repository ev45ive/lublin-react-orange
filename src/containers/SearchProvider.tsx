import React from "react"
import { SearchViewCtx } from "../views/SearchViewCtx"
import { SearchCtx, search } from "../services"
import { Album } from "../models/Album"

type S = {
    query: string
    results: Album[],

}

export class SearchProvider extends React.Component<{}, S>{
    state = {
        query: '',
        results: [],
    }

    onSearch = (query: string) => {
        this.setState({ query })

        search.searchAlbums(query)
            .then(albums => this.setState({
                results: albums
            }))
    }

    render() {
        return <SearchCtx.Provider value={{
            ...this.state,
            onSearch: this.onSearch
        }}>
            {this.props.children}
        </SearchCtx.Provider>
    }
}