import { PlaylistDetails } from "../components/PlaylistDetails";
import { connect } from "react-redux";
import { Playlist } from "../models/Playlist";
import { PlaylistForm } from "../components/PlaylistForm";
import { playlistsUpdate } from "../reducers/playlists";
import { RouteComponentProps } from "react-router-dom";

// Redux
type State = {
    playlists: {
        items: Playlist[],
        selected: Playlist['id']
    }
}
function getSelectedPlaylist(state: State) {
    return state.playlists.items
        .find(p => p.id === state.playlists.selected)!
}

export const SelectedPlaylist = connect(
    (state: State) => ({
        playlist: getSelectedPlaylist(state)
    })

)(PlaylistDetails)


export const SelectedPlaylistForm = connect(
    (state: State) => ({
        playlist: getSelectedPlaylist(state)
    }),
    dispatch => ({
        onSave(draft: Playlist) {
            dispatch(playlistsUpdate(draft))
        }
    })
)(PlaylistForm)