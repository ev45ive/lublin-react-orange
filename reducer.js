var inc = {type:'INC', payload:1}
var dec = {type:'DEC', payload:1}

var lista = [inc,inc,dec,{}];

lista.reduce( (state, action) => {
    switch(action.type){
        case 'INC': return {...state, counter: state.counter + action.payload }
        case 'DEC': return {...state, counter: state.counter - action.payload }
        default: return state
    }
} ,{
	todos:[], numer:1,
	counter:0
})