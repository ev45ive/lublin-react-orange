cd ..
git clone https:// bitbucket.org/ ev45ive/ lublin-react-orange.git

cd lublin-react-orange
npm install
npm start

===

npm i -g create-react-app

create-react-app nazwa-projektu --template=typescript

npm install bootstrap --save 

===

https://marketplace.visualstudio.com/items?itemName=dsznajder.es7-react-js-snippets

src/views/PlaylistsView.tsx
src/components/ItemsList.tsx
src/components/PlaylistDetails.tsx
src/components/PlaylistForm.tsx

===
src/models/Album.ts

class src/views/SearchView.tsx
    src/components/SearchForm.tsx
    src/components/SearchResults.tsx
        src/components/AlbumCard.tsx

====
# Ajax / REST

npm i --save axios 

===
# Forms

https://jaredpalmer.com/formik/docs/api/field

===
# UI Kits

https://react-bootstrap.github.io/

https://material-ui.com/getting-started/installation/

===
# Private NPM
https://docs.npmjs.com/creating-and-publishing-private-packages
https://docs.npmjs.com/about-private-packages
https://verdaccio.org/

===
# Redux 

npm i --save redux react-redux @types/react-redux

https://www.npmjs.com/package/redux-thunk
https://www.npmjs.com/package/redux-promise
https://redux-saga.js.org/

npm i --save redux-thunk

===
# Router
https://reacttraining.com/react-router/web/guides/quick-start

npm install --save react-router-dom @types/react-router-dom

https://reacttraining.com/react-router/core/api/Redirect
https://reacttraining.com/react-router/core/api/Prompt

===
# IE11

https://github.com/facebook/create-react-app/tree/master/packages/react-app-polyfill

===
# Build

npm run build